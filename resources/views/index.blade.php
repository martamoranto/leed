
@extends('layouts.master')

@section('page_title')
	Dashboard
@endsection

@section('page_meta_tags')
	
@endsection

@section('page_css_files')
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/refactor.css') }}" rel="stylesheet">
@endsection

@section('page_js_files')
	<script src="{{ asset('js/app.js') }}" defer></script>
@endsection

@section('page_content')
	
@endsection