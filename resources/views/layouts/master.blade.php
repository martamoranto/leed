<!DOCTYPE html>
<html lang="en">
	<head>
		<title>@yield('page_title') | Dashboard</title>
		@yield('page_meta_tags')
		<meta charset="UTF-8">
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    @yield('page_css_files')
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	    <link href="css/theme/core-ui-style.css" rel="stylesheet">
	</head>
	<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
		<div id="app">
			@include('layouts.parts.header')
			<div class="app-body">
				@include('layouts.parts.sidebar')

				@include('layouts.parts.main')
				@include('layouts.parts.aside-menu')
			</div>
		</div>
		@yield('page_js_files') 
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="js/theme/coreui.min.js"></script>
		<script src="js/theme/main.js"></script>
	</body>
</html>